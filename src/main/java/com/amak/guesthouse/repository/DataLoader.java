package com.amak.guesthouse.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLoader {

    private RoomRepository roomRepository;

    @Autowired
    public DataLoader(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
        loadUsers();
    }

    private void loadUsers() {
        roomRepository.save(new Room(Long.valueOf(1), "Lavender room", "Pretty room with sea view", null));
        roomRepository.save(new Room(Long.valueOf(2), "Oak room", "Big park side room", null));

    }

}
