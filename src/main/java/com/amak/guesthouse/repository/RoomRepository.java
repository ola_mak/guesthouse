package com.amak.guesthouse.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

    List<Room> findAll();

    @Query("SELECT u FROM Room u WHERE u.id NOT IN (SELECT b.room FROM Booking b)")
    List<Room> freeRoomsWithoutBooking();

    @Query("SELECT u FROM Room u WHERE u.id IN (SELECT b.room FROM Booking b where (b.fromDate > :fromDate OR b.toDate <= :fromDate) AND (b.fromDate >= :toDate OR b.toDate < :toDate)) ")
    List<Room> findFreeRoomsFromDateRange(Date fromDate, Date toDate);


}
