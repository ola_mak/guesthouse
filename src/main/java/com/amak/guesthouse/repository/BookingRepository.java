package com.amak.guesthouse.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Long> {

    @Transactional
    @Modifying
    long deleteBookingByToken(String token);

    @Query("SELECT b FROM Booking b where b.room.id = :id AND (b.fromDate >= :fromDate AND b.toDate <= :toDate)")
    Optional<Booking> findBookingInDateRange(Long id, Date fromDate, Date toDate);

}
