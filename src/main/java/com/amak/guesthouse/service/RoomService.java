package com.amak.guesthouse.service;

import com.amak.guesthouse.dto.RoomDTO;
import com.amak.guesthouse.repository.Room;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface RoomService {
    public List<RoomDTO> getFreeRooms(String fromDate, String toDate) throws ParseException;

    public Room getRoomById(Long id);
}
