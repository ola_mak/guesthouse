package com.amak.guesthouse.service;

import com.amak.guesthouse.Converter;
import com.amak.guesthouse.dto.RoomDTO;
import com.amak.guesthouse.parser.DateParser;
import com.amak.guesthouse.repository.Room;
import com.amak.guesthouse.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class RoomServiceImpl implements RoomService {

    private RoomRepository roomRepository;
    private Converter converter;
    private DateParser dateParser;

    @Autowired
    public RoomServiceImpl(RoomRepository roomRepository, Converter converter, DateParser dateParser) {
        this.roomRepository = roomRepository;
        this.converter = converter;
        this.dateParser = dateParser;
    }

    @Override
    public List<RoomDTO> getFreeRooms(String fromDate, String toDate) throws ParseException {
        List<Room> freeRoomsWithoutBooking = roomRepository.freeRoomsWithoutBooking();
        List<Room> freeRoomsFromDateRange = roomRepository.findFreeRoomsFromDateRange(dateParser.parseStringToDate(fromDate), dateParser.parseStringToDate(toDate));
        freeRoomsWithoutBooking.addAll(freeRoomsFromDateRange);
        return converter.convertRoomEntitiesToDTO(freeRoomsWithoutBooking);
    }

    @Override
    public Room getRoomById(Long id) {
        return roomRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Id does not exists"));
    }
}
