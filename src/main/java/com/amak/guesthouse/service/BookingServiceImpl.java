package com.amak.guesthouse.service;

import com.amak.guesthouse.dto.BookingDTO;
import com.amak.guesthouse.exception.FromDateCannotBeAfterToDateException;
import com.amak.guesthouse.parser.DateParser;
import com.amak.guesthouse.repository.Booking;
import com.amak.guesthouse.repository.BookingRepository;
import com.amak.guesthouse.repository.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.text.ParseException;
import java.util.*;

@Service
public class BookingServiceImpl implements BookingService {

    private BookingRepository bookingRepository;
    private RoomService roomService;
    private DateParser dateParser;

    private SecureRandom random = new SecureRandom();

    @Autowired
    public BookingServiceImpl(BookingRepository bookingRepository, RoomService roomService, DateParser dateParser) {
        this.bookingRepository = bookingRepository;
        this.roomService = roomService;
        this.dateParser = dateParser;
    }

    @Override
    public String bookRoom(BookingDTO bookingDTO, long roomId) throws ParseException, FromDateCannotBeAfterToDateException {
        Date fromDate = dateParser.parseStringToDate(bookingDTO.getFromDate());
        Date toDate = dateParser.parseStringToDate(bookingDTO.getToDate());
        validateDates(fromDate, toDate);

        Room room = roomService.getRoomById(roomId);
        throwExceptionIfRoomIsBooked(room, bookingDTO.getFromDate(), bookingDTO.getToDate());

        String token = generateToken();
        Booking booking = Booking.builder()
                .room(room)
                .emailAddress(bookingDTO.getEmailAddress())
                .fromDate(fromDate)
                .toDate(toDate)
                .token(token)
                .build();
        bookingRepository.save(booking);
        return token;
    }

    private void validateDates(Date from, Date to) throws FromDateCannotBeAfterToDateException {
        if (from.after(to) || from.equals(to)) {
            throw new FromDateCannotBeAfterToDateException();
        }
    }

    private void throwExceptionIfRoomIsBooked(Room room, String fromDate, String toDate) throws ParseException {
        Optional<Booking> booking = bookingRepository.findBookingInDateRange(
                room.getId(), dateParser.parseStringToDate(fromDate), dateParser.parseStringToDate(toDate));
        booking.ifPresent(s -> {
            throw new RoomIsAlreadyBookedException("Room with given id is not available within given period");
        });
    }

    @Override
    public void deleteBooking(String token) {
        bookingRepository.deleteBookingByToken(token);
    }

    @Override
    public List<Booking> getBookings() {
        List<Booking> bookings = new ArrayList<>();
        bookingRepository.findAll().forEach(x -> bookings.add(x));
        return bookings;
    }

    private String generateToken() {
        byte bytes[] = new byte[32];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(bytes);
    }

    public List<Booking> getAllBookings() {
        List<Booking> bookings = new ArrayList<>();
        bookingRepository.findAll().forEach(booking -> bookings.add(booking));
        return bookings;
    }
}
