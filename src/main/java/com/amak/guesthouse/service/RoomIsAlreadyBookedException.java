package com.amak.guesthouse.service;

public class RoomIsAlreadyBookedException extends RuntimeException {

    public RoomIsAlreadyBookedException(String message) {
        super(message);
    }
}
