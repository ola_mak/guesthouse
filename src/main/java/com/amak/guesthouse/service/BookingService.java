package com.amak.guesthouse.service;

import com.amak.guesthouse.dto.BookingDTO;
import com.amak.guesthouse.exception.FromDateCannotBeAfterToDateException;
import com.amak.guesthouse.repository.Booking;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface BookingService {
    public String bookRoom(BookingDTO bookingDTO, long roomId) throws ParseException, FromDateCannotBeAfterToDateException;

    public void deleteBooking(String token);

    public List<Booking> getBookings();

    public List<Booking> getAllBookings();
}
