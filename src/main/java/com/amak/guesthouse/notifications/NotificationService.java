package com.amak.guesthouse.notifications;

import com.amak.guesthouse.repository.Booking;
import com.amak.guesthouse.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Service
public class NotificationService {

    private MailService mailService;
    private BookingService bookingService;

    @Autowired
    public NotificationService(MailService mailService, BookingService bookingService) {
        this.mailService = mailService;
        this.bookingService = bookingService;
    }

    @Scheduled(cron = "0 0 23 * * *")
    public void sendNotificationToGuest() {
        bookingService.getAllBookings().stream()
                .filter(booking -> isNotificationDate(booking.getFromDate()))
                .forEach(booking -> sendNotification(booking));
    }

    private boolean isNotificationDate(Date bookingDate) {
        LocalDate currentDate = LocalDate.now();
        LocalDate date = bookingDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return currentDate.equals(date.minusDays(1));
    }

    private void sendNotification(Booking booking) {
        mailService.sendMail(booking.getEmailAddress(), "Your booking date is approaching", "You have one day left to your booking");
    }

}
