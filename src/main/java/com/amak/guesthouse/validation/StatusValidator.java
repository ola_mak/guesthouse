package com.amak.guesthouse.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StatusValidator implements ConstraintValidator<Status, String> {


    private static final Set<String> CATEGORIES = new HashSet<String>
            (Arrays.asList("free"));

    @Override
    public void initialize(Status constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return CATEGORIES.contains(value);
    }

}
