package com.amak.guesthouse.dto;

import com.amak.guesthouse.validation.DateFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {
    @NotBlank(message = "Email Address cannot be null or blank")
    @Email(message = "Email should be valid")
    private String emailAddress;
    @NotBlank(message = "From date cannot be null or blank")
    @DateFormat
    private String fromDate;
    @NotBlank(message = "To date cannot be null or blank")
    @DateFormat
    private String toDate;
}
