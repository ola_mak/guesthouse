package com.amak.guesthouse;

import com.amak.guesthouse.dto.BookingDTO;
import com.amak.guesthouse.dto.RoomDTO;
import com.amak.guesthouse.repository.Booking;
import com.amak.guesthouse.repository.Room;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Converter {

    private static final ModelMapper modelMapper = new ModelMapper();

    public List<RoomDTO> convertRoomEntitiesToDTO(List<Room> rooms) {
        List<RoomDTO> roomsDTO = new ArrayList<>();
        for (Room room : rooms) {
            roomsDTO.add(convertRoomEntityToDTO(room));
        }
        return roomsDTO;
    }

    public RoomDTO convertRoomEntityToDTO(Room room) {
        return modelMapper.map(room, RoomDTO.class);
    }

}
