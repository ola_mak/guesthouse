package com.amak.guesthouse;

import com.amak.guesthouse.dto.BookingDTO;
import com.amak.guesthouse.dto.RoomDTO;
import com.amak.guesthouse.repository.Booking;
import com.amak.guesthouse.service.BookingService;
import com.amak.guesthouse.service.RoomService;
import com.amak.guesthouse.validation.DateFormat;
import com.amak.guesthouse.validation.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@Validated
@RestController
@RequestMapping(path = "/api")
public class GuesthouseController {

    private RoomService roomService;
    private BookingService bookingService;

    @Autowired
    public GuesthouseController(RoomService roomService, BookingService bookingService) {
        this.roomService = roomService;
        this.bookingService = bookingService;
    }

    @GetMapping(value = "/rooms")
    @ResponseStatus(HttpStatus.OK)
    public List<RoomDTO> getFreeRooms(@RequestParam(value = "status", required = true) @Status String status,
                                      @RequestParam(name = "from-date", required = true) @DateFormat String fromDate,
                                      @RequestParam(name = "to-date", required = true) @DateFormat String toDate) throws ParseException {
        return roomService.getFreeRooms(fromDate, toDate);
    }

    // for testing
    @GetMapping(value = "/book")
    public List<Booking> getFreeRooms() {
        return bookingService.getBookings();
    }

    @PostMapping(value = "/rooms/{id}/bookings", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String bookRoom(@PathVariable Long id, @RequestBody @Valid BookingDTO bookingDTO) throws ParseException {
        return bookingService.bookRoom(bookingDTO, id);
    }

    @DeleteMapping(value = "/rooms/bookings")
    @ResponseStatus(HttpStatus.OK)
    public void deleteBooking(@RequestParam(value = "token", required = true) String token) {
        bookingService.deleteBooking(token);
    }
}
