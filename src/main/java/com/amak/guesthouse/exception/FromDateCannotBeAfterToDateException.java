package com.amak.guesthouse.exception;

public class FromDateCannotBeAfterToDateException extends RuntimeException {

    public FromDateCannotBeAfterToDateException() {
        super("From-date cannot be after to-date and dates cannot be equals");
    }
}
