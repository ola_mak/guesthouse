package com.amak.guesthouse.service;

import com.amak.guesthouse.dto.BookingDTO;
import com.amak.guesthouse.exception.FromDateCannotBeAfterToDateException;
import com.amak.guesthouse.parser.DateParser;
import com.amak.guesthouse.repository.Booking;
import com.amak.guesthouse.repository.BookingRepository;
import com.amak.guesthouse.repository.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookingServiceImplTest {

    @Mock
    RoomService roomService;
    @Mock
    BookingRepository bookingRepository;
    @InjectMocks
    BookingServiceImpl bookingServiceImpl;
    @Mock
    DateParser dateParser;


    @Test(expected = RoomIsAlreadyBookedException.class)
    public void shouldThrowExceptionWhenRoomIsBooked() throws ParseException {
        // given
        BookingDTO booking = BookingDTO.builder()
                .emailAddress("a@a.pl")
                .fromDate("20-Jun-2020")
                .toDate("15-Jun-2020")
                .build();
        Room room = Room.builder()
                .id(1l)
                .build();

        // when
        Mockito.when(roomService.getRoomById(any())).thenReturn(room);
        Mockito.when(bookingRepository.findBookingInDateRange(any(), any(), any())).thenReturn(Optional.ofNullable(new Booking()));
        Mockito.when(bookingRepository.save(any())).thenReturn(null);
        Mockito.when(dateParser.parseStringToDate(any())).thenCallRealMethod();

        bookingServiceImpl.bookRoom(booking, 1);

        // then
    }

    @Test(expected = FromDateCannotBeAfterToDateException.class)
    public void shouldThowExceptionWhenValidateDatesTest() throws ParseException {
        // given
        BookingDTO booking = BookingDTO.builder()
                .emailAddress("a@a.pl")
                .fromDate("20-Jun-2020")
                .toDate("15-Jun-2020")
                .build();
        Room room = Room.builder()
                .id(1l)
                .build();
        // when
        Mockito.when(roomService.getRoomById(any())).thenReturn(room);
        Mockito.when(bookingRepository.findBookingInDateRange(any(), any(), any())).thenReturn(Optional.ofNullable(null));
        Mockito.when(bookingRepository.save(any())).thenReturn(null);
        Mockito.when(dateParser.parseStringToDate(any())).thenCallRealMethod();

        bookingServiceImpl.bookRoom(booking, 1);
        // then
    }

}
