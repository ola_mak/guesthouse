package com.amak.guesthouse.parser;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class DateParserTest {

    @InjectMocks
    DateParser dateParser;

    @Test
    void parseStringToDate() throws ParseException {
        //given
        String dateInString = "10-Jun-2020";
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        Date formatedDate = formatter.parse(dateInString);

        //when
        Date date = dateParser.parseStringToDate(dateInString);

        //then
        assert(date).equals(formatedDate);
    }
}
