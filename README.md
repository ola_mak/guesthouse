# PrzegladarkaWiadomosci

#Configuring on your local

1. Put application.properties on the classpath with email username and password. 

#Backend

Backend is spring-boot application written in java8

#Compile code

mvn compile

#Build application

To build and install artifacts into the local repository:

mvn clean install

#Run tests

mvn clean test 

#Run application

To run application use .jar file located in target directory:

java -jar <jar-file-name>.jar
